<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
    public $fillable = ['name','phone'];

    public function groups()
    {
        return $this->belongsToMany('App\Group','contacts_groups');
    }


	public function getGroupListAttribute()
    {
        return $this->groups()->pluck('id')->all();
    }
}
