<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Group;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class ContactController extends Controller
{
    /***/
    public function index(Request $request){

       if(!Auth::check())
            return redirect()->intended('/');

        $contacts = Contact::orderBy('id','DESC')->paginate(10);

        return view('Contact.index',compact('contacts'))

            ->with('i', ($request->input('page', 1) - 1) * 5);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::check())
            return redirect()->intended('/');
        $group = Group::pluck('title', 'id')->all();
        
        return view('Contact.create',compact('group'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        if(!Auth::check())
            return redirect()->intended('/');

        $this->validate($request, [

            'name' => 'required',
            'phone' => 'required',
            'group' => 'required',


        ]);

        $data = $request->all();
        
        $contact = Contact::create($request->all());
        
        $contact->groups()->sync($data['group']);
        
        return redirect()->route('contact.index')

                        ->with('success','Contact created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!Auth::check())
            return redirect()->intended('/');

        $contact = Contact::find($id);

        return view('Contact.show',compact('contact'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::check())
            return redirect()->intended('/');

        $contact = Contact::find($id);

        $group = Group::pluck('title', 'id')->all();

        return view('Contact.edit',compact('contact','group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if(!Auth::check())
            return redirect()->intended('/');

        $this->validate($request, [

            'name' => 'required',
            'phone' => 'required',
            'group' => 'required',


        ]);


        

        $data = $request->all();
        
        $contact = Contact::find($id);
        $contact->update($data);
        $contact->groups()->sync($data['group']);


        return redirect()->route('contact.index')

                        ->with('success','Contact updated successfully');

    }


    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id){

        if(!Auth::check())
            return redirect()->intended('/');

        Contact::find($id)->delete();

        return redirect()->route('contact.index')

                        ->with('success','Contact deleted successfully');

    }
}
