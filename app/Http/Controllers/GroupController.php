<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;


class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request){
        if(!Auth::check())
            return redirect()->intended('/');

        $groups = Group::orderBy('id','DESC')->paginate(10);

        return view('Group.index',compact('groups'))

            ->with('i', ($request->input('page', 1) - 1) * 5);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!Auth::check())
            return redirect()->intended('/');

        return view('Group.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        if(!Auth::check())
            return redirect()->intended('/');

        $this->validate($request, [

            'title' => 'required',


        ]);


        Group::create($request->all());

        return redirect()->route('group.index')

                        ->with('success','Group created successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!Auth::check())
            return redirect()->intended('/');

        $group = Group::find($id);

        return view('Group.show',compact('group'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!Auth::check())
            return redirect()->intended('/');

        $group = Group::find($id);

        return view('Group.edit',compact('group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Group  $group
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if(!Auth::check())
            return redirect()->intended('/');

        $this->validate($request, [

            'title' => 'required',

        ]);


        Group::find($id)->update($request->all());

        return redirect()->route('group.index')

                        ->with('success','Group updated successfully');

    }


    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id){
        if(!Auth::check())
            return redirect()->intended('/');

        Group::find($id)->delete();

        return redirect()->route('group.index')

                        ->with('success','Group deleted successfully');

    }
}
