<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //
    public $fillable = ['title'];

    public function contacts(){
        return $this->belongsToMany('App\Contact','contacts_groups');
    }

}
