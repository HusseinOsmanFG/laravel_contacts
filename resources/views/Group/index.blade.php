@extends('layouts.app')

 

@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>groups CRUD</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ route('group.create') }}"> Create New group</a>

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>No</th>

            <th>Title</th>

            
            <th width="280px">Action</th>

        </tr>

    @foreach ($groups as $key => $group)

    <tr>

        <td>{{ ++$i }}</td>

        <td>{{ $group->title }}</td>

        

        <td>

            <a class="btn btn-info" href="{{ route('group.show',$group->id) }}">Show</a>

            <a class="btn btn-primary" href="{{ route('group.edit',$group->id) }}">Edit</a>

            {!! Form::open(['method' => 'DELETE','route' => ['group.destroy', $group->id],'style'=>'display:inline']) !!}

            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}

            {!! Form::close() !!}

        </td>

    </tr>

    @endforeach

    </table>


    {!! $groups->render() !!}


@endsection