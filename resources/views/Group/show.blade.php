@extends('layouts.app')

 

@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2> Show Group</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ route('group.index') }}"> Back</a>

            </div>

        </div>

    </div>


    <div class="row">


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Title:</strong>

                {{ $group->title }}

            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-contact">

                <strong>Contacts:</strong>
                         <table class="table table-bordered">

                            <tr>
                                <th>name</th>            
                                <th >phone</th>
                            </tr>
                                @foreach ($group->contacts as $contact)

                            <tr>
                            
                            <td><a href="{!! url('contact', ['id' => $contact->id]); !!}">{{ $contact->name }}</a></td>

                            <td>{{ $contact->phone }}</td>
                            
                        </tr>

                        @endforeach

                        </table>

            </div>



        </div>



    </div>


@endsection