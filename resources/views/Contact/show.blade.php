@extends('layouts.app')

 

@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2> Show Contact</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-primary" href="{{ route('contact.index') }}"> Back</a>

            </div>

        </div>

    </div>


    <div class="row">


        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-contact">

                <strong>Name:</strong>

                {{ $contact->name }}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-contact">

                <strong>Phone:</strong>

                {{ $contact->phone }}

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-contact">

                <strong>Groups:</strong>

                @foreach ($contact->groups as $group)
                    <a href="{!! url('group', ['id' => $group->id]); !!}">{{ $group->title }}</a>
                @endforeach

            </div>

        </div>


    </div>


@endsection