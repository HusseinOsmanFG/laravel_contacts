@extends('layouts.app')

 

@section('content')


    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Contacts</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ route('contact.create') }}"> Create New contact</a>

            </div>

        </div>

    </div>


    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif


    <table class="table table-bordered">

        <tr>

            <th>No</th>

            <th>Name</th>

            <th>Phone</th>

            
            <th width="280px">Action</th>

        </tr>

    @foreach ($contacts as $key => $contact)

    <tr>

        <td>{{ ++$i }}</td>

        <td>{{ $contact->name }}</td>

        <td>{{ $contact->phone }}</td>

        

        <td>

            <a class="btn btn-info" href="{{ route('contact.show',$contact->id) }}">Show</a>

            <a class="btn btn-primary" href="{{ route('contact.edit',$contact->id) }}">Edit</a>

            {!! Form::open(['method' => 'DELETE','route' => ['contact.destroy', $contact->id],'style'=>'display:inline']) !!}

            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}

            {!! Form::close() !!}

        </td>

    </tr>

    @endforeach

    </table>


    {!! $contacts->render() !!}


@endsection